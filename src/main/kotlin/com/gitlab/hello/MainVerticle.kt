package com.gitlab.hello

import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj

class MainVerticle : AbstractVerticle() {

  override fun stop(stopFuture: Future<Void>) {
    super.stop()
  }

  override fun start(startPromise: Promise<Void>) {

    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())

    router.get("/").handler { context ->
      context.response().putHeader("content-type", "application/json;charset=UTF-8")
        .end(
          json { obj("message" to "👋 Hello World 🌍🎃") }.encodePrettily()
        )
    }

    router.post().handler { context ->
      val name = context.bodyAsJson?.get<String>("name") ?: "John Doe"

      context.response().putHeader("content-type", "application/json;charset=UTF-8")
        .end(
          json { obj("message" to "👋 Hello $name") }.encodePrettily()
        )
    }

    val httpPort = System.getenv("PORT")?.toInt() ?: 8080

    vertx
      .createHttpServer()
      .requestHandler(router)
      .listen(httpPort) { http ->
        when {
          http.failed() -> {
            startPromise.fail(http.cause())
          }
          http.succeeded() -> {
            println("🌍 Knative application started on port ${httpPort}")
            startPromise.complete()
          }
        }
      }
  }
}
