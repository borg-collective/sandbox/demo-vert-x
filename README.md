# Knative Vert.x web application

This project is a Knative [Serverless Application](https://docs.gitlab.com/ee/user/project/clusters/serverless/#deploying-serverless-applications)

To deploy this application:

- You must activate the [Kubernetes integration](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html) in GitLab
- Then, you need to install [Knative via GitLab’s Kubernetes integration](https://docs.gitlab.com/ee/user/project/clusters/serverless/#installing-knative-via-gitlabs-kubernetes-integration)
- You can use [your own installation of Knative](https://docs.gitlab.com/ee/user/project/clusters/serverless/#using-an-existing-installation-of-knative)

When the CI/CD pipeline is complete, you can test the application:

> You can retrieve the url of the application
>
> - By selecting the `Operations/Serverless` menu
> - Or by selecting the `Operations/Environments` menu

## GET Request

```bash
curl -H "Content-Type: application/json" \
http://${CI_PROJECT_NAME}.${CI_PROJECT_NAME}-${CI_PROJECT_ID}-production.${KUBE_INGRESS_BASE_DOMAIN}
```

> Result:
> `{"message" : "👋 Hello World 🌍"}

## POST Request

```bash
curl -d '{"name":"Bob Morane"}' \
-H "Content-Type: application/json" \
-X POST http://${CI_PROJECT_NAME}.${CI_PROJECT_NAME}-${CI_PROJECT_ID}-production.${KUBE_INGRESS_BASE_DOMAIN}
```

> Result:
> `{"message" : "👋 Hello Bob Morane"}

```bash
curl -H "Content-Type: application/json" \
-X POST http://${CI_PROJECT_NAME}.${CI_PROJECT_NAME}-${CI_PROJECT_ID}-production.${KUBE_INGRESS_BASE_DOMAIN}
```

> Result:
> `{"message" : "👋 Hello John Doe"}
